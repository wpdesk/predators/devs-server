<?php

/* Path to the WordPress codebase you'd like to test. Add a forward slash in the end. */
define( 'ABSPATH', dirname( __FILE__ ) . '/src/' );

/*
 * Path to the theme to test with.
 *
 * The 'default' theme is symlinked from test/phpunit/data/themedir1/default into
 * the themes directory of the WordPress installation defined above.
 */
define( 'WP_DEFAULT_THEME', 'default' );

// Test with multisite enabled.
// Alternatively, use the tests/phpunit/multisite.xml configuration file.
// define( 'WP_TESTS_MULTISITE', true );

// Force known bugs to be run.
// Tests with an associated Trac ticket that is still open are normally skipped.
// define( 'WP_TESTS_FORCE_KNOWN_BUGS', true );

// Test with WordPress debug mode (default).
define( 'WP_DEBUG', true );

// ** MySQL settings ** //

// This configuration file will be used by the copy of WordPress being tested.
// wordpress/wp-config.php will be ignored.

// WARNING WARNING WARNING!
// These tests will DROP ALL TABLES in the database with the prefix named below.
// DO NOT use a production database or one that is shared with something else.

define( 'DB_NAME', 'mysql' );
define( 'DB_USER', 'root' );
define( 'DB_PASSWORD', 'mysql' );
define( 'DB_HOST', 'mysql' );
define( 'DB_CHARSET', 'utf8' );
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 */
define('AUTH_KEY',         'R-SX?%5;U*EH^@b kWp}fM681NK^9 h]N&4Fv3IdHr%Zke3S 6cc16r173Y#,Aig');
define('SECURE_AUTH_KEY',  'xZ#~nj=ojxh{$M/b%s#gH~Y1PbJVe@|nyz{w`xq(BW(gCNjPMIM11:r&zyB+?jX,');
define('LOGGED_IN_KEY',    'D|Zh-s7%P[&NjNQ.M^=`TB&Wv=szQ1fz9xZ?wGFg:OyjIc0tL()^Owr_!+M47DbX');
define('NONCE_KEY',        ')+lrmog(ytKu]iPMtU/4+GeGQMfs,d@,O+p>[2^q#%/|%qd=OAop2KoP*Nj{r2o@');
define('AUTH_SALT',        '$ulR[-+8emMvZpNl@.(xnz@9*J(Ep&A+I2JyK`.bxc6*,f[1JYdE%])B![)*GG&2');
define('SECURE_AUTH_SALT', 'Mp#bBg%+aqsl2V3Na~unC?XKSZ~#~oNC(Pbio${JJ[V*}]x,X4PgqQXFW2/bH*_A');
define('LOGGED_IN_SALT',   '5uG|sA:8sMeIQ91cU{pL%CCz@~v-pyW`lfpcGt>4CYIMx~;1M~&TN n)&?V}Yod4');
define('NONCE_SALT',       '7A| 96oPhZBYp3/#b5a;GuLEF[&st`W(3yGU)JR@VGJ=$X LJY:VGjb952,k$~6Q');

$table_prefix  = 'wpt_';

define( 'WP_TESTS_DOMAIN', 'phpunit.grola.pl' );
define( 'WP_TESTS_EMAIL', 'admin@seostudio.pl' );
define( 'WP_TESTS_TITLE', 'Test Blog' );

define( 'WP_PHP_BINARY', 'php' );

define( 'WPLANG', '' );
