#!/bin/bash

#export SELENIUM_PORT=4445
#export USER_SUFFIX="gr"

export DEPENDENT_PLUGINS_DIR="${HOME}/work/woo/wp-content/plugins"
export WP_DEVELOP_DIR="/home/grola/work/lib/wordpress-develop"
export WC_DEVELOP_DIR="/home/grola/work/lib/woocommerce/plugins/woocommerce"

export APACHE_DOCUMENT_ROOT="${HOME}/work/wptests"
export WOOTESTS_IP="wptests${USER_SUFFIX}.octolize.dev"
export TEST_SITE_WP_URL="https://${WOOTESTS_IP}"
export TEST_DB_NAME="wptests${USER_SUFFIX}"
export TEST_DB_USER="wptests${USER_SUFFIX}"
export TEST_DB_PASSWORD="wptests${USER_SUFFIX}"
export MYSQL_DBNAME=$TEST_DB_NAME
export MYSQL_DBUSER=$TEST_DB_USER
export MYSQL_DBPASSWORD=$TEST_DB_PASSWORD
export TEST_SITE_DB_NAME=$TEST_DB_NAME
export TEST_SITE_DB_USER=$TEST_DB_USER
export TEST_SITE_DB_PASSWORD=$TEST_DB_PASSWORD
export PATH="$PATH:~/work/devs-server/bin"
export COMPOSE_PROJECT_NAME=$USER_SUFFIX
