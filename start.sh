#export PHP=7.2
#export PHP=7.4
#export PHP=8.1
export PHP=8.2

. ~/.profile

docker-compose build --no-cache
docker-compose up -d --force-recreate
